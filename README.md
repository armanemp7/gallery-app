# Gallery app

Initially developed by Canonical, now gallery app is a UBports core app mantained by the community.
Current developers are: Emanuele Sorce <emanuele.sorce@hotmail.com>

## Compiling

Is suggested to use the [Clickable](github.com/bhdouglass/clickable) tool created by Brian Douglass

## Licensing

The app is licensed under the GNU GPL v3 license, see source code for more details